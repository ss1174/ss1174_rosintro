#!/usr/bin/env python3
"""
Created on Sat Oct  1 17:22:03 2022

@author: ss1174, with aid from https://ros-planning.github.io/moveit_tutorials/doc/move_group_python_interface/move_group_python_interface_tutorial.html
"""
#%%% Imports
import numpy as np 
import rospy as rpy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list 

#%%% Robot set-up
print("Running write_initials.py -----------------------------------------------")
moveit_commander.roscpp_initialize(sys.argv)
rpy.init_node("node_1", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
display_trajectory_publisher = rpy.Publisher(
        "/move_group/display_planned_path", 
        moveit_msgs.msg.DisplayTrajectory, 
        queue_size=50
        )
planning_frame = move_group.get_planning_frame()
eef_link = move_group.get_end_effector_link()
group_names = robot.get_group_names()

#%%% Sample code for adding a box to the end effector, not used for the midterm project
def add_pen(): #Adding object in gripper
    box_pose = geometry_msgs.msg.PoseStamped()
    box_pose.header.frame_id = "ur5_gripper"
    box_pose.pose.orientation.w = 1.0
    box_pose.pose.position.z = 0.11  
    box_name = "pen"
    scene.add_box(box_name, box_pose, size=(0.075, 1.075, 0.075))
    grasping_group = "gripper"
    touch_links = robot.get_link_names(group=grasping_group)
    scene.attach_box(eef_link, box_name, touch_links=touch_links)

#%%% Getting info about robot -- for debugging and stuff 
def debug(array):
    #print("============ Printing robot state")
    #print(robot.get_current_state())
    for i in array:
        print(i)
        print(type(i))
    
def traj(plan):
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    display_trajectory_publisher.publish(display_trajectory)

#%%% Planning 
#end effector path points 
s_y = -1*np.linspace(-5*np.pi/12, 5*np.pi/12, 10)
s_x = -1 - np.sin(3*s_y)

e1_y = np.zeros(10)
e1_x = np.linspace(0, 1, 10)
e2_x = np.linspace(1, 0, 10)
e2_y = (0.25 - (e2_x-0.5)**2)**(0.5)
e3_x = np.linspace(0, 6/7, 10)
e3_y = -(0.25 - (e3_x-0.5)**2)**(0.5)

path_var_array = [[s_x, s_y], [e1_x, e1_y], [e2_x, e2_y], [e3_x, e3_y]]

def pose_planning(x_array, y_array, index):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 0.0
    pose_goal.position.x = 0.0
    pose_goal.position.y = x_array[index]
    pose_goal.position.z = y_array[index]
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    print(success)
    move_group.stop()
    move_group.clear_pose_targets()
    

def yz_cartesian_path(x_array, y_array, index): #cartesian path based on end effector path established above
    waypoints = []
    scale = 1.0
    wpose = move_group.get_current_pose().pose
    wpose.position.y += scale * (x_array[index] - wpose.position.y)  
    wpose.position.z += scale * (y_array[index] - wpose.position.z)  
    waypoints.append(copy.deepcopy(wpose))
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.01)  # waypoints to follow  # eef_step # jump_threshold
    #move_group.execute(plan, wait=True)
    return plan

def joint_planning(goal_tuple):
    goal_array = np.array([goal_tuple])
    for goal in goal_array:
        joint_goal = np.zeros(np.size(goal))
        current_joint = goal
        #print(current_joint)
        for i in range(np.size(current_joint)):
            joint_goal[i] = current_joint[i]
        success = move_group.go(joint_goal, wait=True)
        print(success)
        move_group.stop()

#%%% Function calls
if __name__ == "__main__":
    origin = [-0.060578732274697344, -1.9496330435465392, 1.7287279473786494, 0.22089697075149228, -0.061111510632356314, 0.0007040915538709669]
    move_group.go(origin, wait=True) #start point from when moveit initializes
    for var_pair in path_var_array:
        x_var = var_pair[0]
        y_var = var_pair[1]
        for i in range(len(x_var)):
            #pose_planning(x_var, y_var, i)
            plan = yz_cartesian_path(x_var, y_var, i)
            joint_target_array = plan.joint_trajectory.points[-1].positions;
            debug([joint_target_array])
            joint_planning(joint_target_array)
            #traj(plan)