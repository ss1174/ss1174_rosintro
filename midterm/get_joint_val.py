#!/usr/bin/env python3
"""
get_joint_val.py
Created on Thu Oct  6 19:36:45 2022
@author: ss1174
"""
#%%% Imports
import numpy as np 
import rospy as rpy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list 

#%%% Robot set-up
moveit_commander.roscpp_initialize(sys.argv)
rpy.init_node("node_1", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
display_trajectory_publisher = rpy.Publisher(
        "/move_group/display_planned_path", 
        moveit_msgs.msg.DisplayTrajectory, 
        queue_size=50
        )

#%%% Joint goal planning -- ran this independent of everything above 
joint_goal = move_group.get_current_joint_values()
print("------------------------------------------------------------------")
print(joint_goal)
print("------------------------------------------------------------------")