#!/usr/bin/env python3
"""
write_last_name.py
Created on Thu Oct 6 19:36:45 2022
@author: ss1174
"""
#%%% Imports
import numpy as np 
import rospy as rpy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list 

#%%% Robot set-up commands -- allows script to interface with Moveit
moveit_commander.roscpp_initialize(sys.argv)
rpy.init_node("node_1", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
display_trajectory_publisher = rpy.Publisher(
        "/move_group/display_planned_path", 
        moveit_msgs.msg.DisplayTrajectory, 
        queue_size=50
        )

#%%% Goal setting -- all relevant points for planning
zero = [0, -np.pi/2, 0, 0, +np.pi/2, 0]
origin = [0.0004490450013765468, 0, -9.574210425800089e-05, 0.0006836265774294858, -2.518230687531542e-05, 3.0352314580817108e-05]
goal1 = [-0.060578732274697344, -1.9496330435465392, 1.7287279473786494, 0.22089697075149228, -0.061111510632356314, 0.0007040915538709669]
goal2 = [-0.08373372088402409, -1.5239225242406693, 1.8282897554907684, -0.3042644964328556, -0.08343267905800378, 0.0013094910115132308]
goal3 = [0.05194419518744109, -1.9062267320273358, 2.3817607924245223, -0.47410660696472284, 0.052150830901046774, 0.00010132740753299174]
goal4 = [0.02007633140680465, -1.0503755641897747, 1.8077089042217835, -0.7543046531823112, 0.020202160195135654, -0.0015863354799110496]
goal5 = [0.1810389225923812, -1.079290460655674, 1.312784057334226, -0.23356097001541798, 0.18063115410197383, 0.013528490241692559]
goal6 = [0.3112421409035804, -1.2872612795009895, 1.293961487165043, -0.006866004189130415, 0.3106952622559991, 0.013604230109929283]
goal7 = [0.25916930859595233, -1.4578321786014046, 1.811890114641045, -0.3540925725029087, 0.25879380012584896, 0.013504802872506438]
goal8 = [0.27297294940832195, -1.3253791552072993, 1.8920199318088287, -0.566778503002447, 0.2724291509903791, 0.013454302009949792]
goal9 = [0.17994299549715098, -0.9327272500943247, 0.37312008759426174, 0.5594492119775234, 0.17950844448261805, 0.013401526188661528]
goal10 = [0.18177485931112347, -0.9135882035649132, 1.5287357034510807, -0.6152598894303694, 0.18122555098588222, 0.0135880043570209]

#%%% Organizing the goals into letters
S = [goal1, goal2, goal3, goal4]
E = [goal5, goal6, goal5, goal7, goal5, goal8, goal5]
K = [goal9, goal5, goal6, goal5, goal8, goal5, goal10]

#%%% Arrays of variables for later use
var_array = [S, E, K] #to tell the main function call which letters to draw
starting_pose_array = [goal1, goal5, goal9] #used for pulling out the key poses to then screenshot

#%%% Function definitions
def report_q_val(q_array): #getting Q values with respect to the DH table found
    q_array[1] += np.pi/2
    q_array[4] -= np.pi/2
    return q_array

def joint_planning(goal_array): #function that actually moves the robot to each joint goal
    for goal in goal_array:
        '''
        current_joint = goal
        print(current_joint)
        for i in range(len(current_joint)):
            joint_goal[i] = current_joint[i]
        '''
        success = move_group.go(goal, wait=True)
        if success == False: 
            print(success) 
            break;
        move_group.stop()

def key_poses(): #To get screenshots of the key poses
    move_group.go(origin, wait=True)
    for pose in starting_pose_array:
        move_group.go(pose, wait=True)
        print("Q for report: " + str(report_q_val(pose)))
        input("Press Enter to continue...")
    move_group.go(origin, wait=True)

#%%% Function calls
if __name__ == "__main__":
    move_group.go(origin, wait=True) #start point from when moveit initializes
    for x in var_array:
        joint_planning(x)
    #key_poses()
    move_group.go(origin, wait=True) #start point from when moveit initializes