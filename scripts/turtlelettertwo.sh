#!/usr/bin/bash

rosservice call /spawn 7 5 0.2 ""

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \-- '[0.0, 5.0, 0.0]' '[0.0, 0.0, 4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \-- '[0.0, 5.0, 0.0]' '[0.0, 0.0, -4.5]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \-- '[0.0, 5.0, 0.0]' '[0.0, 0.0, 5.5]' 


